const aliveNeighbour = (grid, x, y) => {
  if (Array.isArray(grid) && Number.isInteger(x) && Number.isInteger(y)) {
    let counter = 0;
    for (let row = x - 1; row <= x + 1; row++) {
      for (let col = y - 1; col <= y + 1; col++) {
        if (row >= 0 && row < grid.length && col >= 0 && col < grid[row].length && !(row === x && col === y) && grid[row][col] === true) {
          counter += 1;
        }
      }
    }
    return counter;
  } else {
    return -1;
  }
};

module.exports = aliveNeighbour;
