const { expect } = require('chai');
const aliveNeighbour = require('../src/aliveNeighbour');

const grid = [
  [false, false, false, false],
  [false, true, true, false],
  [false, true, false, false],
  [false, false, true, false],
];

describe('aliveNeighbour', () => {
  describe('for invalid inputs', () => {
    it('should return -1 for no input', () => {
      expect(aliveNeighbour()).to.be.eq(-1);
    });
    it('should return -1 for no input for y', () => {
      expect(aliveNeighbour(grid, -1)).to.be.eq(-1);
    });
    it('should return -1 for no input for x', () => {
      expect(aliveNeighbour(grid)).to.be.eq(-1);
    });
  });

  describe('for valid inputs', () => {
    it('should return 1 for 0,0', () => {
      expect(aliveNeighbour(grid, 0, 0)).to.be.eq(1);
    });
    it('should return 2 for 0,1', () => {
      expect(aliveNeighbour(grid, 0, 1)).to.be.eq(2);
    });
    it('should return 2 for 0,2', () => {
      expect(aliveNeighbour(grid, 0, 2)).to.be.eq(2);
    });
    it('should return 1 for 0,3', () => {
      expect(aliveNeighbour(grid, 0, 3)).to.be.eq(1);
    });
    it('should return 2 for 1,0', () => {
      expect(aliveNeighbour(grid, 1, 0)).to.be.eq(2);
    });
    it('should return 2 for 1,1', () => {
      expect(aliveNeighbour(grid, 1, 1)).to.be.eq(2);
    });
    it('should return 2 for 1,2', () => {
      expect(aliveNeighbour(grid, 1, 2)).to.be.eq(2);
    });
    it('should return 1 for 1,3', () => {
      expect(aliveNeighbour(grid, 1, 3)).to.be.eq(1);
    });
    it('should return 2 for 2,0', () => {
      expect(aliveNeighbour(grid, 2, 0)).to.be.eq(2);
    });
    it('should return 3 for 2,1', () => {
      expect(aliveNeighbour(grid, 2, 1)).to.be.eq(3);
    });
    it('should return 4 for 2,2', () => {
      expect(aliveNeighbour(grid, 2, 2)).to.be.eq(4);
    });
    it('should return 2 for 2,3', () => {
      expect(aliveNeighbour(grid, 2, 3)).to.be.eq(2);
    });
    it('should return 1 for 3,0', () => {
      expect(aliveNeighbour(grid, 3, 0)).to.be.eq(1);
    });
    it('should return 2 for 3,1', () => {
      expect(aliveNeighbour(grid, 3, 1)).to.be.eq(2);
    });
    it('should return 1 for 3,2', () => {
      expect(aliveNeighbour(grid, 3, 2)).to.be.eq(1);
    });
    it('should return 1 for 3,3', () => {
      expect(aliveNeighbour(grid, 3, 3)).to.be.eq(1);
    });
  });
});
