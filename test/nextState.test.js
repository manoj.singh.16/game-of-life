const chai = require('chai');
const nextState = require('../src/nextState');
const { expect } = chai;

// Any live cell with fewer than two live neighbours dies, as if by underpopulation.
// Any live cell with two or three live neighbours lives on to the next generation.
// Any live cell with more than three live neighbours dies, as if by overpopulation.
// Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

const ALIVE = true;
const DEAD = false;

describe('nextState', () => {
  it('Any live cell with fewer than two live neighbours dies, as if by underpopulation. ', () => {
    let currentState = ALIVE;
    let aliveNeighbour = 1;
    let expectedState = DEAD;
    expect(nextState(currentState, aliveNeighbour)).to.be.eq(expectedState);
    aliveNeighbour = 0;
    expect(nextState(currentState, aliveNeighbour)).to.be.eq(expectedState);
  });

  it('Any live cell with two or three live neighbours lives on to the next generation.', () => {
    let currentState = ALIVE;
    let aliveNeighbour = 2;
    let expectedState = ALIVE;
    expect(nextState(currentState, aliveNeighbour)).to.be.eq(expectedState);
    aliveNeighbour = 3;
    expect(nextState(currentState, aliveNeighbour)).to.be.eq(expectedState);
  });

  it('Any live cell with more than three live neighbours dies, as if by overpopulation', () => {
    let currentState = ALIVE;
    let aliveNeighbour = 4;
    let expectedState = DEAD;
    expect(nextState(currentState, aliveNeighbour)).to.be.eq(expectedState);
    aliveNeighbour = 5;
    expect(nextState(currentState, aliveNeighbour)).to.be.eq(expectedState);
    expect(nextState(currentState, 6)).to.be.eq(expectedState);
    expect(nextState(currentState, 7)).to.be.eq(expectedState);
    expect(nextState(currentState, 8)).to.be.eq(expectedState);
  });

  it('Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction', () => {
    expect(nextState(DEAD, 3)).to.be.eq(ALIVE);
    expect(nextState(DEAD, 0)).to.be.eq(DEAD);
    expect(nextState(DEAD, 1)).to.be.eq(DEAD);
    expect(nextState(DEAD, 2)).to.be.eq(DEAD);
    expect(nextState(DEAD, 4)).to.be.eq(DEAD);
    expect(nextState(DEAD, 5)).to.be.eq(DEAD);
    expect(nextState(DEAD, 7)).to.be.eq(DEAD);
    expect(nextState(DEAD, 8)).to.be.eq(DEAD);
  });
});
